/*
** malloc.h for malloc in /home/da-sil_l/Depos/malloc/PSU_2014_malloc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Fri Feb  6 08:37:54 2015 Clement Da Silva
** Last update Fri Feb  6 11:54:38 2015 Clement Da Silva
*/

#ifndef MALLOC_H_
# define MALLOC_H_

# include <stdlib.h>

# define ISLAST(x) *(char *)(x)
# define PSIZE(x) *(size_t *)((x) + sizeof(char))
# define BLOCSIZE(x) *(size_t *)((x) + sizeof(char) + sizeof(size_t))
# define ISFREE(x) *(char *)((x) + sizeof(size_t) * 2 + sizeof(char))
# define METASIZE (sizeof(size_t) * 2 + (sizeof(char) * 2))

int	allocate_pages(size_t);
int	allocate_on_freed(void **, size_t);
void	free(void *);
void	show_mem_alloc();
void	*malloc(size_t);
void	*calloc(size_t, size_t);
void	*realloc(void *, size_t);
void	*metadata_write(void *, size_t, size_t);

#endif	/* !MALLOC_H_ */

