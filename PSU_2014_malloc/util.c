/*
** util.c for klfksdm'kl=f in /home/da-sil_l/Depos/malloc/PSU_2014_malloc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Mon Feb  2 16:59:12 2015 Clement Da Silva
** Last update Thu Feb 12 19:18:27 2015 Joseph Barbisan
*/

#include <unistd.h>
#include "malloc.h"

int		allocate_on_freed(void **ret, size_t size)
{
    size_t	previous_size;

    ISFREE(*ret) = '1';
    previous_size = BLOCSIZE(*ret);
    BLOCSIZE(*ret) = 0 ^ size;
    if (size < previous_size)
      {
	if (ISLAST(*ret) == '1')
	  {
	    ISLAST(*ret) = '0';
	    ISLAST(*ret + size + METASIZE) = '1';
	  }
	else
	  ISLAST(*ret + size + METASIZE) = '0';
	*ret += size + METASIZE;
	PSIZE(*ret) = 0 ^ size;
	BLOCSIZE(*ret) = 0 ^ (previous_size - METASIZE - size);
	ISFREE(*ret) = '0';
	if (ISLAST(*ret) != '1')
	  PSIZE(*ret + BLOCSIZE(*ret) + METASIZE) = BLOCSIZE(*ret);
	*ret -= size;
	return (1);
      }
    *ret += METASIZE;
    return (1);
}

void		*metadata_write(void *ret, size_t size, size_t prevsize)
{
  ISLAST(ret) = ('1');
  PSIZE(ret) = 0 ^ prevsize;
  BLOCSIZE(ret) = 0 ^ (size);
  ISFREE(ret) = ('1');
  return (ret + METASIZE);
}

int		allocate_pages(size_t size)
{
  int		page_nbr;
  int		tmp;

  page_nbr = 0;
  tmp = getpagesize();
  if ((size + METASIZE) % tmp == 0)
    page_nbr = (size + METASIZE) / tmp;
  else
    page_nbr = (size + METASIZE) / tmp + 1;
  if (sbrk(page_nbr * tmp) == (void *)-1)
    return (-1);
  return (page_nbr);
}
