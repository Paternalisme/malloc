/*
** free.c for malloc in /home/barbis_j/Documents/Projets/malloc/PSU_2014_malloc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Jan 29 14:27:15 2015 Joseph Barbisan
** Last update Fri Feb 13 13:47:44 2015 Clement Da Silva
*/

#include <string.h>
#include <stdio.h>
#include "malloc.h"

void		free(void *ptr)
{
  void		*tmp;

  if (ptr && ISFREE(ptr - METASIZE) == '1')
    {
      ptr = ptr - METASIZE;
      ISFREE(ptr) = '0';
      if (ISLAST(ptr) == '0')
      	{
      	  if (PSIZE(ptr) != 0 && ISFREE(ptr - (PSIZE(ptr) + METASIZE)) == '0')
      	    {
      	      while (PSIZE(ptr) != 0 && ISFREE(ptr) == '0') {
      		ptr -= (PSIZE(ptr) + METASIZE);
      	      }
      	      ptr += BLOCSIZE(ptr) + METASIZE;
      	    }
      	  tmp = ptr;
      	  ptr += BLOCSIZE(ptr) + METASIZE;
      	  while (ISLAST(ptr) == '0' && ISFREE(ptr) == '0')
      	    {
      	      BLOCSIZE(tmp) += BLOCSIZE(ptr) + METASIZE;
      	      ptr += BLOCSIZE(ptr) + METASIZE;
      	    }
      	  PSIZE(ptr) = 0 ^ BLOCSIZE(tmp);
      	}
    }
}
