/*
** malloc.c for malloc in /home/barbis_j/rendu/PSU_2014_malloc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Tue Jan 27 17:52:00 2015 Joseph Barbisan
** Last update Sun Feb 15 19:43:41 2015 Joseph Barbisan
*/

#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include "malloc.h"

void		*g_heap_start;

static int	init_page(int *page_count, void **ret, size_t size)
{
  int		tmp;

  if (size == 0)
    return (-1);
  if ((g_heap_start = sbrk(0)) == (void *)(-1))
    return (-1);
  if ((tmp = allocate_pages(size)) == -1)
    return (-1);
  *page_count += tmp;
  *ret = g_heap_start;
  return (0);
}

static int	malloc_loop(void **ret, size_t size,
			    int *page_count, size_t *prevsize)
{
  size_t	totalsize;
  int		tmp;

  totalsize = 0;
  while (ISLAST(*ret) != '1')
    {
      if (ISFREE(*ret) == '0' && (BLOCSIZE(*ret) == size ||
      				  BLOCSIZE(*ret) > size + METASIZE + 1))
      	return (allocate_on_freed(ret, size));
      totalsize += BLOCSIZE(*ret) + METASIZE;
      *ret += (BLOCSIZE(*ret) + METASIZE);
    }
  totalsize += BLOCSIZE(*ret) + METASIZE;
  if (size + METASIZE > (getpagesize() * (*page_count)) - totalsize)
    {
      if ((tmp = allocate_pages(size)) == -1)
	return (-1);
      *page_count += tmp;
    }
  ISLAST(*ret) = ('0');
  *prevsize = BLOCSIZE(*ret);
  *ret += (BLOCSIZE(*ret) + METASIZE);
  return (0);
}

void		*malloc(size_t size)
{
    static int	page_count = 0;
    void	*ret;
    int		tmp;
    size_t	prevsize;

    if (((size + METASIZE) < size) || ((intptr_t)size < 0))
      return (NULL);
    prevsize = 0;
    if (page_count == 0)
      {
	if (init_page(&page_count, &ret, size) == -1)
	  return (NULL);
      }
    else
      {
	ret = g_heap_start;
	tmp = malloc_loop(&ret, size, &page_count, &prevsize);
	if (tmp != 0)
	  return (((tmp == 1) ? ret : NULL));
      }
    return (metadata_write(ret, size, prevsize));
}

void		show_mem_alloc()
{
  void		*heap_ptr;

  printf("break: %p\n", sbrk(0));
  if (g_heap_start)
    {
      heap_ptr = g_heap_start;
      while (ISLAST(heap_ptr) != ('1'))
	{
	  if (ISFREE(heap_ptr) == ('1'))
	    printf("%p - %p : %lu octets\n", heap_ptr + METASIZE,
		   heap_ptr + BLOCSIZE(heap_ptr) + METASIZE, BLOCSIZE(heap_ptr));
	  heap_ptr = heap_ptr + BLOCSIZE(heap_ptr) + METASIZE;
	}
      printf("%p - %p : %lu octets\n", heap_ptr + METASIZE,
	     heap_ptr + BLOCSIZE(heap_ptr) + METASIZE, BLOCSIZE(heap_ptr));
    }
}
