/*
** calloc.c for dff in /home/da-sil_l/Depos/malloc/PSU_2014_malloc
** 
** Made by Clement Da Silva
** Login   <da-sil_l@epitech.net>
** 
** Started on  Mon Feb  2 16:55:34 2015 Clement Da Silva
** Last update Fri Feb  6 08:44:37 2015 Clement Da Silva
*/

#include <string.h>
#include "malloc.h"

void		*calloc(size_t size, size_t bytes)
{
  void		*ret;

  ret = malloc(size * bytes);
  if (ret != NULL)
    memset(ret, 0, size * bytes); 
  return (ret);
}
