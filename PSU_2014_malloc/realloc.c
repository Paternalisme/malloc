/*
** realloc.c for malloc in /home/barbis_j/Documents/Projets/malloc/PSU_2014_malloc
** 
** Made by Joseph Barbisan
** Login   <barbis_j@epitech.net>
** 
** Started on  Thu Jan 29 14:27:28 2015 Joseph Barbisan
** Last update Fri Feb 13 14:14:28 2015 Clement Da Silva
*/

#include <string.h>
#include "malloc.h"

static void	*my_memcpy(void *ret, void *ptr, size_t size)
{
  size_t	i;
  size_t	tmp;

  i = 0;
  if (size >= BLOCSIZE(ptr - METASIZE))
    tmp = BLOCSIZE(ptr - METASIZE);
  else
    tmp = size;
  while (i < tmp)
    {
      *(char *)ret = *(char *)ptr;
      ++ret;
      ++ptr;
      ++i;
    }
  ret -= tmp;
  ptr -= tmp;
  free(ptr);
  return (ret);
}

void		*realloc(void *ptr, size_t size)
{
  void		*ret;

  if (size == 0 && ptr)
    {
      free(ptr);
      return (ptr);
    }
  if (ptr)
    {
      ret = malloc(size);
      ret = my_memcpy(ret, ptr, size);
      return (ret);
    }
  return (malloc(size));
}
